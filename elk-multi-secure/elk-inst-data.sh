helm upgrade \
 --wait \
 --timeout=600 \
 --install \
 --set volumeClaimTemplate.storageClassName=nfs-client \
 --set replicas=3 \
 --set resources.requests.cpu=200m \
 --set resources.requests.memory=8Gi \
 --set resources.limits.cpu=6000m \
 --set resources.limits.memory=8Gi \
 --set esJavaOpts="-Xmx4g -Xms4g" \
 --values ./data.yml \
 elasticsearch-data \
 elastic/elasticsearch \

