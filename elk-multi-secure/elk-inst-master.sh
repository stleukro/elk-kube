helm upgrade \
 --wait \
 --timeout=600 \
 --install \
 --set persistence.enabled=false \
 --set volumeClaimTemplate.storageClassName=nfs-client \
 --set replicas=3 \
 --set resources.requests.cpu=200m \
 --set resources.requests.memory=8Gi \
 --set resources.limits.cpu=6000m \
 --set resources.limits.memory=8Gi \
 --set esJavaOpts="-Xmx4g -Xms4g" \
 --values ./master.yml \
 elasticsearch-master \
 elastic/elasticsearch \

