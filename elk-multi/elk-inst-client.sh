helm upgrade \
 --wait \
 --timeout=600 \
 --install \
 --set persistence.enabled=false \
 --set volumeClaimTemplate.storageClassName=nfs-client \
 --set replicas=3 \
 --set service.type=LoadBalancer \
 --values ./client.yml \
 elasticsearch-coordinator \
 elastic/elasticsearch \

