helm install  elastic/elasticsearch \
 --name elasticsearch \
 --version 7.2.0 \
 --set volumeClaimTemplate.storageClassName=nfs-client \
 --set replicas=5 \
 --set resources.requests.cpu=200m \
 --set resources.requests.memory=8Gi \
 --set resources.limits.cpu=6000m \
 --set resources.limits.memory=8Gi \
 --set esJavaOpts="-Xmx4g -Xms4g" \
 --set service.type=NodePort \
 --values /root/rke/k8scl7/elk/values.yaml \

