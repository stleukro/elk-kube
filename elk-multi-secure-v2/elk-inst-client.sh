helm upgrade \
 --wait \
 --timeout=600 \
 --install \
 --set image=registry.gitlab.com/stleukro/elk-kube/my-elastic \
 --set imageTag=7.2.0 \
 --set persistence.enabled=false \
 --set volumeClaimTemplate.storageClassName=nfs-client \
 --set replicas=3 \
 --set service.type=LoadBalancer \
 --values ./client.yml \
 elasticsearch-coordinator \
 elastic/elasticsearch \

