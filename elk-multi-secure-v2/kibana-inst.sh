helm install  elastic/kibana \
 --name kibana \
 --version 7.2.0 \
 --set service.type=LoadBalancer \
 --values ./kibana-values.yaml \


